package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.rate;

import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.NbpApiException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.Currency;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.NbpRate;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.NbpResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceTest {

    private ExchangeRateService exchangeRateService;

    @Mock
    private HttpNbpService nbpService;

    @Before
    public void setUp() {
        this.exchangeRateService = new ExchangeRateServiceImpl(nbpService);
    }

    @Test
    public void shouldTakeRateFromApi() {
        //Given
        when(nbpService.getRate(Currency.USD.getNpbName())).thenReturn(buildNbpApiResponse());

        //When
        BigDecimal rate = exchangeRateService.getRate(Currency.USD, Currency.PLN);

        //Then
        assertTrue(new BigDecimal("0.2632").compareTo(rate) == 0);
    }

    @Test(expected = NbpApiException.class)
    public void shouldGetApiException() {
        //Given
        doThrow(new NbpApiException("NBP api exception")).when(nbpService).getRate(Currency.USD.getNpbName());

        try {
            //When
            exchangeRateService.getRate(Currency.PLN, Currency.USD);
        } catch (NbpApiException e) {
            //Then
            assertEquals("NBP api exception", e.getMessage());
            verify(nbpService, times(1)).getRate(Currency.USD.getNpbName());
            throw e;
        }
    }

    private NbpResponse buildNbpApiResponse() {
        NbpRate rate = new NbpRate();
        rate.setRate(BigDecimal.valueOf(3.8));
        NbpResponse response = new NbpResponse();
        response.setRates(List.of(rate));
        return response;
    }

}