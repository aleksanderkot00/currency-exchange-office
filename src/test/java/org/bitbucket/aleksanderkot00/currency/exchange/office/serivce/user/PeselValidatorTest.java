package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.PeselException;
import org.junit.Assert;
import org.junit.Test;

@Slf4j
public class PeselValidatorTest {

    @Test
    public void shouldReturnOk() {
        //Given
        String pesel = "69082265635";

        //When
        PeselValidator.validatePesel(pesel);

        //Then
        log.info("Pesel is correct");
    }

    @Test(expected = PeselException.class)
    public void shouldReturnFormException() {
        //Given
        String pesel = "69082265634";

        try {
            //When
            PeselValidator.validatePesel(pesel);
        } catch (PeselException e) {
            //Then
            Assert.assertEquals("Pesel is not correct {pesel: 69082265634}", e.getMessage());
            throw e;
        }
    }

    @Test(expected = PeselException.class)
    public void shouldReturnAgeException() {
        //Given
        String pesel = "04292713273";

        try {
            //When
            PeselValidator.validatePesel(pesel);
        } catch (PeselException e) {
            //Then
            Assert.assertEquals("User too young", e.getMessage());
            throw e;
        }
    }

}