package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.exchange;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.AccountBalanceException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.Currency;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.ExchangeRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.Subaccount;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository.UserRepository;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.rate.ExchangeRateService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class ExchangeServiceTest {

    private ExchangeService exchangeService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ExchangeRateService exchangeRateService;

    @Before
    public void setUp() {
        this.exchangeService = new ExchangeServiceImpl(exchangeRateService, userRepository);
    }

    @Test
    public void shouldExchange() {
        //When
        ExchangeRequest request = buildExchangeRequest();
        when(userRepository.findByIdWithSubaccounts(19L)).thenReturn(buildUser());
        when(exchangeRateService.getRate(Currency.PLN, Currency.USD)).thenReturn(BigDecimal.valueOf(3.8));

        //When
        exchangeService.exchange(19L, request);

        //Then
        log.info("Exchanged money");
    }

    @Test(expected = AccountBalanceException.class)
    public void shouldNotExchange() {
        //When
        ExchangeRequest request = buildWrongExchangeRequest();
        when(userRepository.findByIdWithSubaccounts(19L)).thenReturn(buildUser());

        try {
            //When
            exchangeService.exchange(19L, request);
        } catch (AccountBalanceException e) {
            //Then
            Assert.assertEquals("Account balance too low", e.getMessage());
            throw e;
        }
    }

    private Optional<User> buildUser() {
        User user = User.builder()
                .pesel("69082265635")
                .name("name")
                .lastname("lastname")
                .build();
        Subaccount subaccount = new Subaccount(user, BigDecimal.valueOf(100), Currency.PLN);
        List<Subaccount> subaccounts = new ArrayList<>();
        subaccounts.add(subaccount);
        user.setSubaccounts(subaccounts);
        return Optional.of(user);
    }


    private ExchangeRequest buildExchangeRequest() {
        return new ExchangeRequest(Currency.PLN, Currency.USD, BigDecimal.TEN);
    }

    private ExchangeRequest buildWrongExchangeRequest() {
        return new ExchangeRequest(Currency.PLN, Currency.USD, BigDecimal.valueOf(120));
    }

}