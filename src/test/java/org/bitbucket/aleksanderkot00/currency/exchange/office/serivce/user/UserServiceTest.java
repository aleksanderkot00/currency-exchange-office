package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user;

import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.PeselException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.UserNotFoundException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.Currency;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.SubaccountResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserRegistrationRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        this.userService = new UserServiceImpl(userRepository, passwordEncoder);
    }

    @Test
    public void shouldRegisterUser() {
        //Given
        when(passwordEncoder.encode("password")).thenReturn("epassword");
        when(userRepository.findByPesel("69082265635")).thenReturn(Optional.empty());

        //When
        UserRegistrationRequest request = buildRegisterRequest();
        UserResponse response = userService.registerUser(request);

        //Then
        assertEquals(request.getPesel(), response.getPesel());
        assertEquals(request.getName(), response.getName());
        assertEquals(request.getLastname(), response.getLastname());
        assertTrue(request.getPlnInitValue().compareTo(extractPlnValue(response)) == 0);
    }

    @Test(expected = PeselException.class)
    public void shouldUserPeselExist() {
        //Given
        when(userRepository.findByPesel("69082265635")).thenReturn(Optional.of(new User()));
        UserRegistrationRequest request = buildRegisterRequest();

        try {
            //When
            userService.registerUser(request);
        } catch (PeselException e) {
            //Then
            assertEquals("Pesel already exists {pesel: 69082265635}", e.getMessage());
            throw e;
        }
    }

    @Test
    public void shouldFindUser() {
        //Given
        String pesel = "69082265635";
        when(userRepository.findByPeselWithSubaccounts(pesel)).thenReturn(buildUser());

        //When
        UserResponse response = userService.getByPesel(pesel);

        //Then
        assertEquals(pesel, response.getPesel());
        assertEquals("name", response.getName());
        assertEquals("lastname", response.getLastname());
    }

    @Test(expected = UserNotFoundException.class)
    public void shouldNotFindUser() {
        //Given
        String pesel = "69082265635";
        when(userRepository.findByPeselWithSubaccounts(pesel)).thenReturn(Optional.empty());

        try {
            //When
            userService.getByPesel(pesel);
        } catch (UserNotFoundException e) {
            //Then
            assertEquals("User not found {pesel: 69082265635}", e.getMessage());
            throw e;
        }
    }

    private BigDecimal extractPlnValue(UserResponse response) {
        return response.getSubaccounts().stream()
                .filter(s -> s.getCurrency().equals(Currency.PLN))
                .map(SubaccountResponse::getBalance)
                .findFirst()
                .orElse(null);
    }

    private UserRegistrationRequest buildRegisterRequest() {
        return new UserRegistrationRequest(
                "69082265635",
                "name",
                "lastname",
                "password",
                BigDecimal.TEN
        );
    }

    private Optional<User> buildUser() {
        User user = User.builder()
                .pesel("69082265635")
                .name("name")
                .lastname("lastname")
                .subaccounts(Collections.emptyList())
                .build();
        return Optional.of(user);
    }

}