package org.bitbucket.aleksanderkot00.currency.exchange.office.controller;

import lombok.RequiredArgsConstructor;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserRegistrationRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/register")
@RequiredArgsConstructor
public class RegisterController {

    private final UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse registerUser(@NotNull @Valid @RequestBody UserRegistrationRequest request) {
        return userService.registerUser(request);
    }

}
