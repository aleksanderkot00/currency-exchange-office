package org.bitbucket.aleksanderkot00.currency.exchange.office.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PeselException extends RuntimeException {

    public PeselException(String message) {
        super(message);
    }

}
