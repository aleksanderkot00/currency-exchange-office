package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user;

import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.PeselException;

import java.time.LocalDate;

import static java.lang.Character.getNumericValue;
import static java.lang.String.format;

class PeselValidator {

    private static final String PESEL_VALIDATION_NUMBER = "1379137913";
    private static final int ADULT_AGE = 18;

    static void validatePesel(String pesel) {
        validateFormat(pesel);
        validateAge(pesel);
    }

    private static void validateFormat(String pesel) {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum = sum + getNumericValue(pesel.charAt(i)) * getNumericValue(PESEL_VALIDATION_NUMBER.charAt(i));
        }
        sum = ((sum % 10) == 0)
                ? 0
                : 10 - (sum % 10);
        if (!(sum == getNumericValue(pesel.charAt(10)))) {
            throw new PeselException(format("Pesel is not correct {pesel: %s}", pesel));
        }
    }

    private static void validateAge(String pesel) {
        int dayOfBirth = Integer.parseInt(pesel.substring(4,6));
        int monthOfBirth = Integer.parseInt(pesel.substring(2,4));
        int yearOfBirth = Integer.parseInt(pesel.substring(0,2));
        LocalDate birthDate = (monthOfBirth > 12)
                ? LocalDate.of(2000 + yearOfBirth, monthOfBirth - 20, dayOfBirth)
                : LocalDate.of(1900 + yearOfBirth, monthOfBirth, dayOfBirth);
        if (LocalDate.now().minusYears(ADULT_AGE).isBefore(birthDate)) {
            throw new PeselException("User too young");
        }
    }

}
