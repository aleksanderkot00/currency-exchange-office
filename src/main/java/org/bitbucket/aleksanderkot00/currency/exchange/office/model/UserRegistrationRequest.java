package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "password")
public class UserRegistrationRequest {

    @NotNull
    @Pattern(regexp = "[0-9]{11}")
    private String pesel;

    @NotNull
    private String name;

    @NotNull
    private String lastname;

    @NotNull
    private String password;

    @DecimalMin(value = "0")
    private BigDecimal plnInitValue;

}
