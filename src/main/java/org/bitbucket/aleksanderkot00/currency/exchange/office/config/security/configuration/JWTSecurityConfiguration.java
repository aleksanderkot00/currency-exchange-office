package org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.configuration;

import lombok.RequiredArgsConstructor;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.jwt.JwtHeaderFilter;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.utils.SecuredPathSelector;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.jwt.JwtService;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
public class JWTSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final JwtService jwtService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors()
                .and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/login", "/swagger-resources",
                        "/swagger-resources/**", "/configuration/ui",
                        "/configuration/security", "/swagger-ui.html",
                        "/webjars/**")
                .permitAll()
                .antMatchers(SecuredPathSelector.getSecuredPathMatchers())
                .authenticated()
                .and().exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(new JwtHeaderFilter(jwtService), UsernamePasswordAuthenticationFilter.class);

    }

}
