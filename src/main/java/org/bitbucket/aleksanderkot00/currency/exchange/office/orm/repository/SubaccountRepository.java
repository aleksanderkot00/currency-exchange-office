package org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository;

import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.Subaccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubaccountRepository extends JpaRepository<Subaccount, Long> {
}
