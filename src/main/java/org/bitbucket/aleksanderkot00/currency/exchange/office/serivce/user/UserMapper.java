package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user;

import org.bitbucket.aleksanderkot00.currency.exchange.office.model.SubaccountResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserRegistrationRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.Subaccount;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;

import java.util.List;
import java.util.stream.Collectors;

class UserMapper {

    static User toUser(UserRegistrationRequest request, String encryptedPassword) {
        return User.builder()
                .pesel(request.getPesel())
                .password(encryptedPassword)
                .name(request.getName())
                .lastname(request.getLastname())
                .build();
    }

    static UserResponse toResponse(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .pesel(user.getPesel())
                .name(user.getName())
                .lastname(user.getLastname())
                .subaccounts(toSubaccountResponses(user.getSubaccounts()))
                .build();
    }

    private static List<SubaccountResponse> toSubaccountResponses(List<Subaccount> subaccounts) {
        return subaccounts.stream()
                .map(s -> new SubaccountResponse(s.getBalance(), s.getCurrency()))
                .collect(Collectors.toList());
    }

}
