package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@ToString
public class UserResponse {

    private final Long id;

    private final String pesel;

    private final String name;

    private final String lastname;

    private final List<SubaccountResponse> subaccounts;

}
