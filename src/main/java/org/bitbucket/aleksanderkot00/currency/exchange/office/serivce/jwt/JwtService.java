package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.jwt;

import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user.AuthUser;

public interface JwtService {

    String generateToken(Long userId);

    boolean validateToken(String token);

    AuthUser extractUserFromToken(String token);

}
