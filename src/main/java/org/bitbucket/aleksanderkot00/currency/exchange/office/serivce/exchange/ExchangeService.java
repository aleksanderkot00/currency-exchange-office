package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.exchange;

import org.bitbucket.aleksanderkot00.currency.exchange.office.model.ExchangeRequest;

public interface ExchangeService {

    void exchange(Long userId, ExchangeRequest request);

}
