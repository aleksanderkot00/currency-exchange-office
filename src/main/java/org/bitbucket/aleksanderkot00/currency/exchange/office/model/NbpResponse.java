package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class NbpResponse {

    private List<NbpRate> rates;

}