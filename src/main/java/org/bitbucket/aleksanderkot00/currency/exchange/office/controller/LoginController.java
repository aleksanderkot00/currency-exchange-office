package org.bitbucket.aleksanderkot00.currency.exchange.office.controller;

import lombok.RequiredArgsConstructor;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.LoginRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.LoginResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.login.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public LoginResponse login(@NotNull @Valid @RequestBody LoginRequest request) {
        return loginService.authenticate(request);
    }

}
