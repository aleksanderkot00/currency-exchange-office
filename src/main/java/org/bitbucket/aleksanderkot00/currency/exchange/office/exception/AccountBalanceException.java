package org.bitbucket.aleksanderkot00.currency.exchange.office.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class AccountBalanceException extends RuntimeException {

    public AccountBalanceException(String message) {
        super(message);
    }

}
