package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.rate;

import org.bitbucket.aleksanderkot00.currency.exchange.office.model.NbpResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "nbp-api", path = "/api/exchangerates/rates/a/")
interface HttpNbpService {

    @RequestMapping(method = RequestMethod.GET, value = "/{currency}?format=json")
    NbpResponse getRate(@PathVariable("currency") String currency);

}
