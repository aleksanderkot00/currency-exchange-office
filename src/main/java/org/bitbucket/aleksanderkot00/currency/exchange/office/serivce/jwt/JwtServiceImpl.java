package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user.AuthUser;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user.UserRole;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.AuthenticationException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
class JwtServiceImpl implements JwtService {

    private static final String ROLES_KEY = "role";

    private final JwtProperties jwtProperties;

    @Override
    public String generateToken(Long userId) {

        Instant instant = LocalDateTime.now().plusHours(1).toInstant(ZoneOffset.UTC);

        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtProperties.getSecretKey());
            return buildToken(userId, instant, algorithm);
        } catch (JWTCreationException e) {
            log.error("Failed to generate token {userId: {}}", userId, e);
            throw new AuthenticationException(format("Failed to generate token {userId: %s}", userId));
        }
    }

    @Override
    public boolean validateToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtProperties.getSecretKey());
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(jwtProperties.getIssuer()).build();
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException e) {
            log.error("Failed to parse token", e);
            throw new AuthenticationException(format("Failed to parse token {exception: %s}", e.getMessage()));
        }
    }

    @Override
    public AuthUser extractUserFromToken(String token) {
        return AuthUser.builder()
                .id(Long.parseLong(Objects.requireNonNull(extractUserIdFromToken(token))))
                .roles(extractRolesFromToken(token))
                .build();
    }

    private String buildToken(Long userId, Instant instant, Algorithm algorithm) {
        return JWT.create()
                .withSubject(userId.toString())
                .withClaim(ROLES_KEY, UserRole.ROLE_USER.name())
                .withExpiresAt(Date.from(instant))
                .withIssuer(jwtProperties.getIssuer())
                .sign(algorithm);
    }

    private String extractUserIdFromToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getSubject();
        } catch (JWTDecodeException e) {
            log.error("Can't decode token " + token, e);
            throw new AuthenticationException(format("Failed to decode token {exception: %s}", e.getMessage()));
        }
    }

    private List<String> extractRolesFromToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return List.of(jwt.getClaim(ROLES_KEY).asString());
        } catch (JWTDecodeException e) {
            log.error("Can't decode token " + token, e);
            return new ArrayList<>();
        }
    }

}
