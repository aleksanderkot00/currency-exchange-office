package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user;

import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserRegistrationRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserResponse;

public interface UserService {

    UserResponse registerUser(UserRegistrationRequest registrationRequest);

    UserResponse getByPesel(String pesel);

}
