package org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.jwt;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user.AuthUser;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.utils.SecuredPathSelector;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.jwt.JwtService;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
@Slf4j
public final class JwtHeaderFilter extends OncePerRequestFilter {

    private static final String BEARER_PREFIX = "BEARER ";
    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest servletRequest, @NonNull HttpServletResponse servletResponse, @NonNull FilterChain filterChain) throws IOException, ServletException {
        Thread.currentThread().setName("jwtHeaderFilter_" + servletRequest.getRequestURI());

        String path= servletRequest.getServletPath();
        String token = SecuredPathSelector.isPathSecured(path)
                ? getToken(servletRequest)
                : null;

        if (ofNullable(token).isPresent() && jwtService.validateToken(token)) {

            AuthUser user = jwtService.extractUserFromToken(token);
            List<GrantedAuthority> grantedAuthorities = user.getRoles().stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
            log.info("Current user: {}", user);

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                    = new UsernamePasswordAuthenticationToken(user, "", grantedAuthorities);

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private String getToken(HttpServletRequest httpRequest) {
        String tokenWithBearerPrefix = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (tokenWithBearerPrefix == null || !tokenWithBearerPrefix.toUpperCase().startsWith(BEARER_PREFIX)) {
            log.warn("Authorization header does not start with 'Bearer ' prefix: {}", tokenWithBearerPrefix);
            return null;
        }
        return tokenWithBearerPrefix.substring(BEARER_PREFIX.length());
    }

}
