package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class NbpRate {

    @JsonProperty("mid")
    private BigDecimal rate;

}