package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.PeselException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.UserNotFoundException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.Currency;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserRegistrationRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.Subaccount;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository.UserRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public UserResponse registerUser(UserRegistrationRequest request) {
        log.trace("Registering user {userRegistrationRequest: {}}", request);
        PeselValidator.validatePesel(request.getPesel());
        String encodedPassword = passwordEncoder.encode(request.getPassword());
        User user = UserMapper.toUser(request, encodedPassword);
        Subaccount plnInitSubaccount = new Subaccount(user, request.getPlnInitValue(), Currency.PLN);
        user.setSubaccounts(List.of(plnInitSubaccount));
        saveUser(request, user);
        UserResponse response = UserMapper.toResponse(user);
        log.info("Registered user {userResponse: {}}", response);
        return response;
    }

    @Override
    public UserResponse getByPesel(String pesel) {
        log.trace("Getting user by pesel {pesel: {}}", pesel);
        User user = getUser(pesel);
        UserResponse response = UserMapper.toResponse(user);
        log.info("Got user by pesel {userResponse: {}}", response);
        return response;
    }

    private User getUser(String pesel) {
        return userRepository.findByPeselWithSubaccounts(pesel)
                .orElseThrow(() -> new UserNotFoundException(format("User not found {pesel: %s}", pesel)));
    }

    private void saveUser(UserRegistrationRequest request, User user) {
        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            log.error("Pesel already exists {pesel: {}}", request.getPesel());
            throw new PeselException(format("Pesel already exists {pesel: %s}", request.getPesel()));
        }
    }

}
