package org.bitbucket.aleksanderkot00.currency.exchange.office.controller;

import lombok.RequiredArgsConstructor;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user.role.IsRoleUser;
import org.bitbucket.aleksanderkot00.currency.exchange.office.config.utils.AuthUtils;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.ExchangeRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.exchange.ExchangeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/exchange")
@RequiredArgsConstructor
public class ExchangeController {

    private final ExchangeService exchangeService;

    @IsRoleUser
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void login(@NotNull @Valid @RequestBody ExchangeRequest request) {
        exchangeService.exchange(AuthUtils.getAuthUserId(), request);
    }

}
