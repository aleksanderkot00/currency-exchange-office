package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.rate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.NbpApiException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.Currency;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.NbpRate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
class ExchangeRateServiceImpl implements ExchangeRateService {

    private final HttpNbpService nbpService;

    @Override
    public BigDecimal getRate(Currency from, Currency to) {
        log.trace("Getting exchange rate {from: {}, to: {}}", from, to);
        try {
            BigDecimal fromRate = getRate(from);
            BigDecimal toRate = getRate(to);
            BigDecimal response = toRate.divide(fromRate, 4, RoundingMode.CEILING);
            log.info("Got exchange rate {from: {}, to: {}, rate: {}}", from, to, response);
            return response;
        } catch (RestClientException e) {
            log.error("NBP api exception {message: {}}", e.getMessage());
            throw new NbpApiException(format("NBP api exception {message: %s}", e.getMessage()));
        }
    }

    private BigDecimal getRate(Currency currency) {
        return currency.equals(Currency.PLN)
                ? BigDecimal.ONE
                : getRateFromApi(currency);
    }

    private BigDecimal getRateFromApi(Currency currency) {
        return nbpService.getRate(currency.getNpbName()).getRates().stream()
                .map(NbpRate::getRate)
                .findAny()
                .orElseThrow(() -> new NbpApiException(format("Cannot extract rate {currency: %s}", currency)));
    }

}
