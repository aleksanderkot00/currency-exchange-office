package org.bitbucket.aleksanderkot00.currency.exchange.office.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class NbpApiException extends RuntimeException {

    public NbpApiException(String message) {
        super(message);
    }

}
