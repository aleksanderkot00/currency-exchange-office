package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Component
@ConfigurationProperties(prefix = "jwt")
@Data
@Validated
class JwtProperties {

    @NotBlank
    private String issuer;

    @NotBlank
    private String secretKey;

}
