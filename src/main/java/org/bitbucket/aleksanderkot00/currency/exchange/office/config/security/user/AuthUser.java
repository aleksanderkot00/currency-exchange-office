package org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@ToString
public class AuthUser {

    private final Long id;

    private final List<String> roles;

}
