package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {

    @NotNull
    private String pesel;

    @NotNull
    private String password;

}

