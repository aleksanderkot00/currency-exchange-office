package org.bitbucket.aleksanderkot00.currency.exchange.office.config.utils;

import org.bitbucket.aleksanderkot00.currency.exchange.office.config.security.user.AuthUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthUtils {

    public static Long getAuthUserId() {
        return getAuthUser().getId();
    }

    private static AuthUser getAuthUser() {
        return (AuthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
