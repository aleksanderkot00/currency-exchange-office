package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.login;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.AuthenticationException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.LoginRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.LoginResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository.UserRepository;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.jwt.JwtService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    @Override
    public LoginResponse authenticate(LoginRequest loginRequest) {
        log.info("Authentication of user {pesel: {}}", loginRequest.getPesel());

        User user = userRepository.findByPesel(loginRequest.getPesel())
                .orElseThrow(() -> new AuthenticationException(format("Active user not found {pesel: %s}", loginRequest.getPesel())));

        boolean passwordMatch = checkPassword(user.getPassword(), loginRequest.getPassword());
        if (!passwordMatch) {
            throw new AuthenticationException(format("Wrong password {pesel: %s}", loginRequest.getPesel()));
        }

        String token = jwtService.generateToken(user.getId());
        LoginResponse loginResponse = new LoginResponse(token);

        log.info("Authentication success {pesel: {}}", loginRequest.getPesel());
        return loginResponse;
    }

    private boolean checkPassword(String userPassword, String requestPassword) {
        return passwordEncoder.matches(requestPassword, userPassword);
    }

}
