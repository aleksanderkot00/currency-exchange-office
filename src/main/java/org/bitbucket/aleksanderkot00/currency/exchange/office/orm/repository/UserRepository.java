package org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository;

import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByPesel(String pesel);

    @Query("select u from User u left join fetch u.subaccounts where u.pesel = ?1 ")
    Optional<User> findByPeselWithSubaccounts(String pesel);

    @Query("select u from User u left join fetch u.subaccounts where u.id = ?1 ")
    Optional<User> findByIdWithSubaccounts(Long id);

}
