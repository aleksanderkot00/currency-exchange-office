package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.login;

import org.bitbucket.aleksanderkot00.currency.exchange.office.model.LoginRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.LoginResponse;

public interface LoginService {

    LoginResponse authenticate(LoginRequest loginRequest);

}
