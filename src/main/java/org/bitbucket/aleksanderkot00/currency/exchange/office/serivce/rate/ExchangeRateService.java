package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.rate;

import org.bitbucket.aleksanderkot00.currency.exchange.office.model.Currency;

import java.math.BigDecimal;

public interface ExchangeRateService {

    BigDecimal getRate(Currency from, Currency to);

}
