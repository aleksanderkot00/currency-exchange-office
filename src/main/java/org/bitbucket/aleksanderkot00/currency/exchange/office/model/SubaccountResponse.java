package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
@ToString
public class SubaccountResponse {

    private final BigDecimal balance;

    private final Currency currency;

}
