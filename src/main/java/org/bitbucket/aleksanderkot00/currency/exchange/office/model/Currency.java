package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import lombok.Getter;

public enum Currency {

    PLN("pln"),
    USD("usd");

    Currency(String npbName) {
        this.npbName = npbName;
    }

    @Getter
    private final String npbName;

}
