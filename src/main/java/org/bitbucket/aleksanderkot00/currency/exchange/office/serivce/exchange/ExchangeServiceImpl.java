package org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.exchange;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.AccountBalanceException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.exception.UserNotFoundException;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.ExchangeRequest;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.Subaccount;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.domain.User;
import org.bitbucket.aleksanderkot00.currency.exchange.office.orm.repository.UserRepository;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.rate.ExchangeRateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExchangeServiceImpl implements ExchangeService {

    private final ExchangeRateService exchangeRateService;
    private final UserRepository userRepository;

    @Transactional
    @Override
    public void exchange(Long userId, ExchangeRequest request) {
        log.trace("Exchanging money {userId: {}, request: {}}", userId, request);
        User user = getUser(userId);
        validateBalance(user, request);
        BigDecimal exchangeRate = exchangeRateService.getRate(request.getFrom(), request.getTo());
        withdraw(user, request);
        draw(user, request, exchangeRate);
        log.info("Exchanged money {userId: {}, request: {}}", userId, request);
    }

    private static void withdraw(User user, ExchangeRequest request) {
        user.getSubaccounts().stream()
                .filter(s -> s.getCurrency().equals(request.getFrom()))
                .forEach(s -> calculateBalanceAfterWithdraw(request, s));
    }

    private static void draw(User user, ExchangeRequest request, BigDecimal exchangeRate) {
        Subaccount drawSubaccount = user.getSubaccounts().stream()
                .filter(s -> s.getCurrency().equals(request.getTo()))
                .findAny()
                .orElseGet(() -> createUserSubaccount(user, request));
        drawSubaccount.setBalance(calculateBalanceAfterDraw(request, exchangeRate, drawSubaccount));
    }

    private static void calculateBalanceAfterWithdraw(ExchangeRequest request, Subaccount subaccount) {
        BigDecimal balance = subaccount.getBalance().subtract(request.getValue());
        balance = balance.setScale(2, RoundingMode.CEILING);
        subaccount.setBalance(balance);
    }

    private static BigDecimal calculateBalanceAfterDraw(ExchangeRequest request, BigDecimal exchangeRate, Subaccount subaccount) {
        BigDecimal balance = subaccount.getBalance().add(exchangeRate.multiply(request.getValue()));
        balance = balance.setScale(2, RoundingMode.CEILING);
        return balance;
    }

    private static Subaccount createUserSubaccount(User user, ExchangeRequest request) {
        Subaccount subaccount = new Subaccount(user, BigDecimal.ZERO, request.getTo());
        user.getSubaccounts().add(subaccount);
        return subaccount;
    }

    private static void validateBalance(User user, ExchangeRequest request) {
        if (!hasEnoughMoney(user, request)) {
            throw new AccountBalanceException("Account balance too low");
        }
    }

    private static boolean hasEnoughMoney(User user, ExchangeRequest request) {
        return user.getSubaccounts().stream()
                .filter(s -> s.getCurrency().equals(request.getFrom()))
                .map(Subaccount::getBalance)
                .anyMatch(b -> b.compareTo(request.getValue()) >= 0);
    }

    private User getUser(Long userId) {
        return userRepository.findByIdWithSubaccounts(userId)
                .orElseThrow(() -> new UserNotFoundException(format("User not found {id: %s}", userId)));
    }

}
