package org.bitbucket.aleksanderkot00.currency.exchange.office.controller;

import lombok.RequiredArgsConstructor;
import org.bitbucket.aleksanderkot00.currency.exchange.office.model.UserResponse;
import org.bitbucket.aleksanderkot00.currency.exchange.office.serivce.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/{pesel}")
    @ResponseStatus(HttpStatus.OK)
    public UserResponse getUserById(@PathVariable("pesel") String pesel) {
        return userService.getByPesel(pesel);
    }

}
