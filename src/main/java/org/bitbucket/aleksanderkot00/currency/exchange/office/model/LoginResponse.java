package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class LoginResponse {

    private final String token;

}
