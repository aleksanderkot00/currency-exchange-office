package org.bitbucket.aleksanderkot00.currency.exchange.office;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class CurrencyExchangeOfficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyExchangeOfficeApplication.class, args);
	}

}
