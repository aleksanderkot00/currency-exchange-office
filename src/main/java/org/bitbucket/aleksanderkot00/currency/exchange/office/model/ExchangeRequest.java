package org.bitbucket.aleksanderkot00.currency.exchange.office.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRequest {

    @NotNull
    private Currency from;

    @NotNull
    private Currency to;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal value;

}
