package org.bitbucket.aleksanderkot00.currency.exchange.office.config.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.AntPathMatcher;

import java.util.Arrays;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SecuredPathSelector {

    public static String[] getSecuredPathMatchers() {
        return SECURED_PATH_MATCHERS;
    }

    public static boolean isPathSecured(String path) {
        return Arrays.stream(SECURED_PATH_MATCHERS)
                .anyMatch(securedPath -> new AntPathMatcher()
                        .match(securedPath, path));
    }

    private static final String[] SECURED_PATH_MATCHERS = new String[]{
            "/exchange/**"
    };

}
